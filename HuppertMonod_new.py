# -*- coding: utf-8 -*-
"""
Created on Mon Jul 25 13:58:54 2022

@author: Pia Falter
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy
from modelbase.ode import Model, Simulator
from IPython.utils import io


def constant(x: float) -> float:
    return x


def proportional(x: float, y: float) -> float:
    return x * y


def monod(s: float, mu_max: float, ks: float) -> float:
    return mu_max * (s / (ks + s))

def michment (s: float, mu_max: float, ks: float, km: float) -> float:
    return ks * mu_max * (s / (km + s))

def get_huppert_v1() -> Model:
    """version of Huppert Model with Monod kinetics in uptake term"""
    m1 = Model()
    m1.add_parameters(
        {
            "a": 0.00075,  # nutrient input rate
            "b": 1,  # nutrient uptake by phytoplankton
            "c": 1,  # nutrient uptake by phytoplankton
            "d": 0.1,  # loss rate
            "e": 0,  # loss rate
            #"l": 1.0,
        }
    )
    m1.add_compounds(
        [
            "N",  # limiting nutrient in mg/ m^3
            "P",  # Phytoplankton in g/ m^3
        ]
    )
    #m1.add_derived_parameter("cl", proportional, ["c", "l"])
    m1.add_reaction_from_args("N_input", constant, {"N": 1}, ["a"])
    m1.add_reaction_from_args("N_loss", proportional, {"N": -1}, ["N", "e"])
    m1.add_reaction_from_args("N_uptake", monod, {"N": -1}, ["N", "P", "b"])   
    m1.add_reaction_from_args("P_N_uptake", monod, {"P": 1}, ["N", "P", "c"])   
    m1.add_reaction_from_args("P_loss", proportional, {"P": -1}, ["P", "d"])
    return m1



def get_huppert_v3() -> Model:
    """version of Huppert Model with Michaelis-Menten kinetics in uptake term"""
    m3 = Model()
    m3.add_parameters(
        {
            "a": 0.00075,  # nutrient input rate
            "b": 1,  # nutrient uptake by phytoplankton
            "c": 1,  # nutrient uptake by phytoplankton
            "d": 0.1,  # loss rate
            "e": 0,  # loss rate
            "km": 1,
            #"l": 1.0,
        }
    )
    m3.add_compounds(
        [
            "N",  # limiting nutrient in mg/ m^3
            "P",  # Phytoplankton in g/ m^3
            ]
        )
    m3.add_reaction_from_args("N_input", constant, {"N": 1}, ["a"])
    m3.add_reaction_from_args("N_loss", proportional, {"N": -1}, ["N", "e"])
    m3.add_reaction_from_args("N_uptake_mm", michment, {"N": -1}, ["N", "P", "b", "km"])   
    m3.add_reaction_from_args("P_N_uptake_mm", michment, {"P": 1}, ["N", "P", "c", "km"])   
    m3.add_reaction_from_args("P_loss", proportional, {"P": -1}, ["P", "d"])
    return m3



def get_huppert_v4() -> Model:
    """version of Huppert Model with MichMent-kinetics and recycling term"""
    m4 = Model()
    m4.add_parameters(
        {
            "a": 0.00075,  # nutrient input rate
            "b": 1,  # nutrient uptake by phytoplankton
            "c": 1,  # nutrient uptake by phytoplankton
            "d": 0.1,  # loss rate
            "e": 0,  # loss rate
            "km": 1,
            "f": 0.1,
            #"l": 1.0,
        }
    )
    m4.add_compounds(
        [
            "N",  # limiting nutrient in mg/ m^3
            "P",  # Phytoplankton in g/ m^3
            ]
        )
    m4.add_reaction_from_args("N_input", constant, {"N": 1}, ["a"])
    m4.add_reaction_from_args("N_loss", proportional, {"N": -1}, ["N", "e"])
    m4.add_reaction_from_args("N_gain_recyc", proportional, {"N": 1}, ["P", "f"])
    m4.add_reaction_from_args("N_uptake_mm", michment, {"N": -1}, ["N", "P", "b", "km"])   
    m4.add_reaction_from_args("P_N_uptake_mm", michment, {"P": 1}, ["N", "P", "c", "km"])   
    m4.add_reaction_from_args("P_loss", proportional, {"P": -1}, ["P", "d"])
    return m4

#y0 = {"N": 2, "P": 200.0}
#s = Simulator(get_huppert_v1())
#s.update_parameters({"b": 0})
#s.initialise(y0)
#with io.capture_output():
  #  s.simulate(100)
#s.plot()
#plt.show()
