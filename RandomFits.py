import numpy as np
import matplotlib.pyplot as plt
import scipy
import pandas as pd
import random

from modelbase.ode import Model, Simulator


#m1 = get_huppert_v1()


def read_env_data():
    '''funciton to initialise environmental data set'''
    data_environment = pd.read_table\
            ('./datasetexploration/Paul_Matthiessen_2021/datasets/mesocosm_2014_environment.tab',
             skiprows=30)                          #if use: "elim_whitespace=True", then KeyError with 'M1' in line 39

    # clearify data/ clean them up
    data_environment = data_environment.dropna()
    data_environment.rename(columns = {'Meso label':'Meso'}, inplace=True)
    print(type(data_environment['Meso'][2]))
    data_environment.Meso = data_environment.Meso.str.replace('M', '')
    data_environment.Meso = pd.to_numeric(data_environment.Meso)
    print(type(data_environment['Meso'][2]))
    data_environment['Date/Time'] = pd.to_datetime(data_environment['Date/Time']).dt.date
    # change column with dates as measurement sequence (DOE day)
    # min(data_environment['DOE [day]']) (needed to find shift)
    data_environment['DOE [day]'] += 4

    return data_environment

def read_phyto_data():
    '''function to initialise phytolplankton data set'''
    # import data on phytoplankton -> column Phytopl. C
    data_phyto = pd.read_table\
        ('./datasetexploration/Paul_Matthiessen_2021/datasets/mesocosm_2014_phytoplankton.tab',
        skiprows=31)

    data_phyto = data_phyto.dropna()
    data_phyto.rename(columns={'Meso label': 'Meso'}, inplace=True)
    data_phyto.Meso = pd.to_numeric(data_phyto.Meso)
    data_phyto['Date/Time'] = pd.to_datetime(data_phyto['Date/Time']).dt.date
    # change column with dates as measurement sequence (DOE day)
    #min(data_phyto['DOE [day]'])
    data_phyto['DOE [day]'] += 1            #Anna set originally to 1, changed to 2 to get same timepoints

    return data_phyto


def column_sorter_env(data):
    '''function to sort environmental data according to Meso's, returns list with env. dataframes'''
    env_Meso_dict = {}
    for X in range(0, 13):
        data_env_MX = data.loc[data['Meso'] == X]
        env_Meso_dict[f'M{X}'] = data_env_MX
    return env_Meso_dict


def column_sorter_phyto(data):
    '''funtion to sort phytoplankton data according to Meso's, returns list with phytopl. dataframes'''
    phyto_Meso_dict = {}
    for X in range(0, 13):
        data_pytho_MX = data.loc[data['Meso'] == X]
        phyto_Meso_dict[f'M{X}'] = data_pytho_MX
    return phyto_Meso_dict


'''DATA PREPARATION'''
data_environment = read_env_data()
data_phyto = read_phyto_data()

env_Meso_dict = column_sorter_env(data_environment) #acces Meso 1 from list with env_Meso_list[1]
phyto_Meso_dict = column_sorter_phyto(data_phyto)
#print(env_Meso_list[1])




''''MAIN'''
Mesocosm_dict = {}
for X in range (1,13):
    
    t_exp1 = env_Meso_dict[f'M{X}']['DOE [day]']
    N_exp = env_Meso_dict[f'M{X}']['TDN [µmol/l]']
    t_exp2 = phyto_Meso_dict[f'M{X}']['DOE [day]']
    P_exp = phyto_Meso_dict[f'M{X}']['Phytopl C [µg/l] (inedible phytoplankton C)']
    Mesocosm_dict[f'M{X}'] = {'t_exp1': t_exp1,
                              'N_exp': N_exp, 
                              't_exp2': t_exp2, 
                              'P_exp': P_exp, 
                              }





# import the model
from HuppertMonod_new import get_huppert_v1 
from HuppertMonod_new import get_huppert_v3

#m2 = get_huppert_v1()
m3 = get_huppert_v3()
#print(m1.stoichiometries)
bounds= ((0, None), (0, None), (0, None), (0, None), (0, None), )
model_time_points = np.array([0, 3, 5, 7, 10, 12, 14, 17, 19, 21, 24, 26])
#model_time_points = np.array([0., 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., ])
#model_time_points = np.linspace (0, 700, 12)
print(f"model_time_points = {model_time_points}")



# Define function performing the Fit of the modeldata incl. plotting data and fit
def Time_Fitting_Monod_data (m3, Mesocosm_dict, X, y0_data, set_of_params, iteration):
    N_exp = Mesocosm_dict[f'M{X}']['N_exp'].to_numpy()
    P_exp = Mesocosm_dict[f'M{X}']['P_exp'].to_numpy()
    t_exp1 = Mesocosm_dict[f'M{X}']['t_exp1']
    t_exp2 = Mesocosm_dict[f'M{X}']['t_exp2']
    
    y0 = y0_data            #{"N": 2, "P": 200.0} #originally N=1.0, P=1.0
    
    s = Simulator(m3)
    s.initialise(y0)

    time_points = t_exp1.to_numpy()
   
    # add the solution
    s.update_parameters(set_of_params)
  
    s.initialise(y0)
    s.simulate(time_points = model_time_points)
    #print(s.simulation_parameters)

    fig, ax1 = plt.subplots()

    N_model = s.get_variable('N')
    P_model = s.get_variable('P')
    #if len(N_model) != len(time_points):
     #   N_model, P_model = N_model[1:], P_model[1:]

    ax1.set_xlabel('days passed from the beginning of the experiment')
    ax1.set_ylabel('Nutrients [au]', color='blue')
    ax1.plot(t_exp1, N_exp, color='blue', marker='o')
    ax1.plot(t_exp1, N_model, 'b--')
    ax1.tick_params(axis='y', labelcolor='blue')

    # Adding Twin Axes

    ax2 = ax1.twinx()

    ax2.set_ylabel('Inedible Phytoplankton [au]', color='green')
    ax2.plot(t_exp2, P_exp, color='green', marker='o')
    ax2.plot(t_exp1, P_model, 'g--')            #maybe /5 to get useful order of magnitude
    ax2.tick_params(axis='y', labelcolor='green')
    
    plt.title(f"Iteration: {iteration}      a = {set_of_params['a']}, b = {set_of_params['b']}, \n c = {set_of_params['c']}, d = {set_of_params['d']}, e = {set_of_params['e']}, km = {set_of_params['km']}")
    # Show plot
    plt.savefig(f'./MonteCarlo_randomfits_MM/Mesocosm_iteration{iteration}_fitting.png')
    #plt.show()
    X = N_exp, P_exp, N_model, P_model
    return (X, fig)

# dict with params

mc_params = {"a": None, 
             "b": None,
             "c": None, 
             "d": None,
             "e": None,
             "km": None,
             }

bounds = {"a": 150, 
          "b": 0.8,
          "c": 0.8, 
          "d": 0.6,
          "e": 0.3,
          "km": 1.5,
            }

# Monte Carlo simulation: create x parameter sets, each containing dict with params

def set(parameter):
        if parameter == "a":
           value = round(random.uniform(100, bounds[f'{parameter}']), 4)
        #elif parameter == "b":
         #   value = round(random.uniform(0.3, bounds[f'{parameter}']), 4)
        #elif parameter == "c":
         #   value = round(random.uniform(0.2, bounds[f'{parameter}']), 4)
        else:
            value = round(random.uniform(0, bounds[f'{parameter}']), 4)
        return value
    
param_sets = {}

for i in range (1, 100):
    param_sets[f'set_of_params{i}'] = {}
    for key in mc_params:
        value = set(f"{key}")
        mc_params[f'{key}'] = value   
    param_sets[f'set_of_params{i}'].update(mc_params)
    mc_params = {"a": None, "b": None,"c": None, "d": None,"e": None, "km": None, }

# define function that evaluates qualitiy of fit

def fit_eval (data_points, fit_points): 
     return np.sqrt(np.mean(np.square(data_points - fit_points)))


# plot model with these parameters

y0_dict = {"y1": {"N": 2., "P": 200.0},    # not yet found useful values for fit
           "y2": {"N": 4.5, "P": 320.0}, 
           "y3": {"N": 4.5, "P": 350.0},
           "y4": {"N": 5, "P": 200.0},
           "y5": {"N": 2, "P": 200.0},
           "y6": {"N": 2, "P": 200.0},
           "y7": {"N": 2, "P": 200.0},
           "y8": {"N": 2, "P": 200.0},
           "y9": {"N": 2, "P": 200.0},
           "y10": {"N": 2, "P": 200.0},
           "y11": {"N": 2, "P": 200.0},
           "y12": {"N": 2, "P": 200.0},
           }  

list_of_all_params = []
list_of_best_eval_params = []
list_with_evals = []

for x in range(1,100):
    Time_Fitting_Monod_data(m3, Mesocosm_dict, 1, y0_dict['y1'],         param_sets[f'set_of_params{x}'], x )
    plt.close()
    N_exp, P_exp, N_model, P_model = Time_Fitting_Monod_data(m3, Mesocosm_dict, 1, y0_dict['y1'], param_sets[f'set_of_params{x}'], x )[0]
    list_of_all_params.append(param_sets[f'set_of_params{x}'])
    
    eval_N = fit_eval(N_exp, N_model)
    eval_P = fit_eval(P_exp, P_model)
    overall_eval = (100 * eval_N + eval_P) / 2
        
    list_with_evals.append(overall_eval)


#find 20 best fits (smallest values in list with evals), return plots and parameter values
def best_fit_searcher(list_with_evals, list_of_all_params):
    K = 20
    res = sorted(range(len(list_with_evals)), key = lambda sub: list_with_evals[sub])[:K]
    for element in range(0, K):
        element_value = res[element]
        list_of_best_eval_params.append(list_of_all_params[element_value])
    return res

res = best_fit_searcher(list_with_evals, list_of_all_params)


print(f"res = {res}")
for e,i  in zip(res, range(1,20)):
    print(e)
    print (f"Iteration {e} parameters: {list_of_best_eval_params[i]}")
    fig = Time_Fitting_Monod_data(m3, Mesocosm_dict, 1, y0_dict['y1'], param_sets[f'set_of_params{e}'], e )[1]
    plt.savefig(f'./MonteCarlo_randomfits_MM/best twenty random fits MM/Mesocosm_iteration{e}_fitting.png')
        


print("smallest twenty" + str(res))


#print(list_of_all_params)

# find closest one via minimum-fining?