import numpy as np
import matplotlib.pyplot as plt
import scipy
from modelbase.ode import Model, Simulator

def get_model_v1() -> Model:
    m1 = Model() #Huppert model with first Monod update

    parameters = {
        "a": 0.00075,   # nutrient input rate -> if smaller: larger timescale for same graph, if set higher: faster development
        "b": 1,         # nutrient uptake by phytoplankton -> if smaller: max value phytopl. decreases, if bigger: max value phytopl. increases
        "c": 1,         # nutrient uptake by phytoplankton -> changes nutrient graph, but influence taking on phytopl. (higher = less nutrient)
        "d": 0.1,       # loss rate -> influences whole system procedure
        "e": 0,         # loss rate
        "l": 1.,
        }

    m1.add_parameters(parameters)
    m1.add_compounds(("N", "P"))       # P: Phytoplankton in g/ m^3; N: limiting nutrient in mg/ m^3
    m1.add_derived_parameter("cl", lambda x,y: x*y, ["l","c"])
    # formulate functions


    def N_input(a):
        return a


    def N_uptake(N, P, b):
        return P * (N / (b + N))


    #def c_light(c, l):
        #   return l * c


    def P_N_uptake(P, N, c):
        return P * (N / (c + N))


    def N_loss(N, e):
        return e * N


    def P_loss(P, d):
        return d * P


    #def N_deriv(N, P, a, b, e):
        #   """function that describes: nutrient conc =  input - uptake - loss"""
        #  return a - b * N * P - e * N


    #def P_deriv(P, N, c, d):
        #   """ function that describes: phytoplankton = uptake - death - sinking"""
        #  return c * N * P - d * P


    # set up model reactions

    m1.add_reaction(rate_name="N_input",
                    function=N_input,
                    stoichiometry={"N": 1},
                    parameters=["a"],
                    )

    m1.add_reaction(rate_name="N_loss",
                    function=N_loss,
                    stoichiometry={"N": -1},
                    parameters=["e"],
                    )

    m1.add_reaction(rate_name="N_uptake",
                    function=N_uptake,
                    stoichiometry={"N": -1},
                    modifiers=["P"],
                    parameters=["b"],
                    )

    m1.add_reaction(rate_name="P_N_uptake",
                    function=P_N_uptake,
                    stoichiometry={"P": 1},
                    modifiers=["N"],
                    dynamic_variables=["P", "N"],
                    parameters=["c"],
                    reversible=True,
                    )

    m1.add_reaction(rate_name="P_loss",
                    function=P_loss,
                    stoichiometry={"P": -1},
                    parameters=["d"],
                    )
    
    return m1