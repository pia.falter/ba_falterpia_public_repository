import numpy as np
import matplotlib.pyplot as plt
import scipy
import warnings

from modelbase.ode import Model, Simulator

import pandas as pd
import datetime as dt
from IPython.display import display
from IPython.utils import io
#from lmfit import minimize, Parameters, Parameter, report_fit

from numpy.typing import NDArray
from scipy.optimize import minimize
import csv


# import data on nutrients -> column TDN

def read_env_data():
    '''funciton to initialise environmental data set'''
    data_environment = pd.read_table\
            ('./datasetexploration/Paul_Matthiessen_2021/datasets/mesocosm_2014_environment.tab',
             skiprows=30)                          #if use: "elim_whitespace=True", then KeyError with 'M1' in line 39

    # clearify data/ clean them up
    data_environment = data_environment.dropna()
    data_environment.rename(columns = {'Meso label':'Meso'}, inplace=True)
    print(type(data_environment['Meso'][2]))
    data_environment.Meso = data_environment.Meso.str.replace('M', '')
    data_environment.Meso = pd.to_numeric(data_environment.Meso)
    print(type(data_environment['Meso'][2]))
    data_environment['Date/Time'] = pd.to_datetime(data_environment['Date/Time']).dt.date
    # change column with dates as measurement sequence (DOE day)
    # min(data_environment['DOE [day]']) (needed to find shift)
    data_environment['DOE [day]'] += 4

    return data_environment

def read_phyto_data():
    '''function to initialise phytolplankton data set'''
    # import data on phytoplankton -> column Phytopl. C
    data_phyto = pd.read_table\
        ('./datasetexploration/Paul_Matthiessen_2021/datasets/mesocosm_2014_phytoplankton.tab',
        skiprows=31)

    data_phyto = data_phyto.dropna()
    data_phyto.rename(columns={'Meso label': 'Meso'}, inplace=True)
    data_phyto.Meso = pd.to_numeric(data_phyto.Meso)
    data_phyto['Date/Time'] = pd.to_datetime(data_phyto['Date/Time']).dt.date
    # change column with dates as measurement sequence (DOE day)
    #min(data_phyto['DOE [day]'])
    data_phyto['DOE [day]'] += 1            #Anna set originally to 1 

    return data_phyto


def column_sorter_env(data):
    '''function to sort environmental data according to Meso's, returns list with env. dataframes'''
    env_Meso_dict = {}
    for X in range(0, 13):
        data_env_MX = data.loc[data['Meso'] == X]
        env_Meso_dict[f'M{X}'] = data_env_MX
    return env_Meso_dict


def column_sorter_phyto(data):
    '''funtion to sort phytoplankton data according to Meso's, returns list with phytopl. dataframes'''
    phyto_Meso_dict = {}
    for X in range(0, 13):
        data_pytho_MX = data.loc[data['Meso'] == X]
        phyto_Meso_dict[f'M{X}'] = data_pytho_MX
    return phyto_Meso_dict


'''DATA PREPARATION'''
data_environment = read_env_data()
data_phyto = read_phyto_data()

env_Meso_dict = column_sorter_env(data_environment) #acces Meso 1 from list with env_Meso_list[1]
phyto_Meso_dict = column_sorter_phyto(data_phyto)
#print(env_Meso_list[1])




''''MAIN'''
Mesocosm_dict = {}
for X in range (1,13):
    
    t_exp1 = env_Meso_dict[f'M{X}']['DOE [day]']
    #N_exp = env_Meso_dict[f'M{X}']['TDN [µmol/l]']
    N_TDN_exp = env_Meso_dict[f'M{X}']['TDN [µmol/l]']
    print(f"N_Exp_onlyTDN = {N_TDN_exp}")
    N_Si_exp = env_Meso_dict[f'M{X}']['Si(OH)4 [µmol/l]']
    print(f"N_Exp_onlySilicium = {N_Si_exp}")
    N_exp = env_Meso_dict[f'M{X}']['Si(OH)4 [µmol/l]'] + env_Meso_dict[f'M{X}']['TDN [µmol/l]']
    print((f"N_Exp_SUM = {N_exp}"))
    t_exp2 = phyto_Meso_dict[f'M{X}']['DOE [day]']
    P_exp = phyto_Meso_dict[f'M{X}']['Phytopl C [µg/l] (inedible phytoplankton C)']
    Mesocosm_dict[f'M{X}'] = {'t_exp1': t_exp1,
                              'N_exp': N_exp, 
                              't_exp2': t_exp2, 
                              'P_exp': P_exp, 
                              }

#print(Mesocosm_dict.keys())
#print(Mesocosm_dict['M1']['N_exp'])
#Mesocosm_dict['M5']['t_exp1']= np.append(Mesocosm_dict['M5']['t_exp1'], [26])



# import the model
from HuppertMonod_new import get_huppert_v1, get_huppert_v3, get_huppert_v4


#m1 = get_huppert_v1()
m3 = get_huppert_v3()
#m4 = get_huppert_v4()
#print(m4.stoichiometries)
bounds= ((0, None), (0, None), (0, None), (0, None), (0, None), (0, None), (0, None),)
model_time_points = np.array([0, 3, 5, 7, 10, 12, 14, 17, 19, 21, 24, 26])
#model_time_points = np.array([0., 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., ])
#model_time_points = np.linspace (0, 700, 12)
print(f"model_time_points = {model_time_points}")

#Define function that will fit parameters to the experimental data set
def fit_time_series(
    m: Model,
    p0: dict[str, float],
    y0: dict[str, float],
    data: NDArray,
    time_points: NDArray,
    adj_factor: NDArray,
) -> dict[str, float]:
    par_names = list(p0.keys())
    x0 = list(p0.values())
    assert len(data) == len(time_points)

    
    def residual(par_values: NDArray, data: NDArray, time_points: NDArray, ):
        s = Simulator(m.copy())
        #print(par_values)
        s.update_parameters(dict(zip(par_names, par_values)))
        s.initialise(y0)
        #with io.capture_output():
        _, y = s.simulate(time_points=time_points)
        if y is None:
            return np.inf
        #if y.shape[0] != len(time_points):
         #   y = y[1:,:]
        return np.sqrt(np.mean(np.square((data - y)*adj_factor)))
    
    #def constraint_func(x0):
    #cons = {'type': 'ineq', 'fun': (x0[1]/x0[2]) / (x0[6]/x0[3]) -1}
       # return cons
    #cons=constraint_func(x0)
    
    return dict(zip(par_names, minimize(residual, x0=x0, args=(data, model_time_points), bounds=bounds ).x)) #bounds=bounds



# Define function performing the Fit of the modeldata incl. plotting data and fit
def Time_Fitting_Monod_data (model, Mesocosm_dict, X, y0_data, a1, a2):
    N_exp = Mesocosm_dict[f'M{X}']['N_exp'].to_numpy()
    #N_exp = N_exp * 2.85       #scale data t o.o. magn. of model 03.08.
    P_exp = Mesocosm_dict[f'M{X}']['P_exp'].to_numpy()
    #P_exp = P_exp * 80         #scale data t o.o. magn. of model 03.08.
    t_exp1 = Mesocosm_dict[f'M{X}']['t_exp1']
    #print(f"t_exp1 = {t_exp1}")
    t_exp2 = Mesocosm_dict[f'M{X}']['t_exp2']
    #print(f"t_exp2 = {t_exp2}")
    adj_factor = np.array([a1, a2])
    
    y0 = y0_data            #{"N": 2, "P": 200.0} #originally N=1.0, P=1.0
    
    s = Simulator(model)
    s.initialise(y0)

    time_points = t_exp1.to_numpy()
    #print(f"time_points = {time_points}")
    #assert y is not None

    par_data = fit_time_series(model, {'a': 0.00075 * 4000, 'b': 0.5, 'c': 1., 'd': 0.1, 'e': 0., 'km': 0.1* 4000, 'f':0.05}, y0, np.array([N_exp, P_exp]).T, time_points, adj_factor)
    
    # add the solution
    s.update_parameters(fit_time_series(model, {'a': 0.00075 * 4000, 'b': 0.5, 'c': 1., 'd': 0.1, 'e': 0., 'km': 0.1* 4000, 'f': 0.05}, y0, np.array([N_exp, P_exp]).T, time_points, adj_factor))
    #print(m1.get_parameters())
    s.initialise(y0)
    s.simulate(time_points = model_time_points)
    #print(s.simulation_parameters)

    fig, ax1 = plt.subplots()

    N_model = s.get_variable('N')
    P_model = s.get_variable('P')
    #if len(N_model) != len(time_points):
     #   N_model, P_model = N_model[1:], P_model[1:]

    ax1.set_xlabel('days passed from the beginning of the experiment', fontsize=15)
    ax1.set_ylabel('Silicium [µmol/l]', color='blue', fontsize=15)
    ax1.plot(t_exp1, N_exp, color='blue', marker='o')
    ax1.plot(t_exp1, N_model, 'b--')
    ax1.tick_params(axis='y', labelcolor='blue', labelsize=15)
    ax1.tick_params(axis='x', labelsize=15)

    # Adding Twin Axes

    ax2 = ax1.twinx()

    ax2.set_ylabel('Inedible Phytoplankton [µg/l]', color='green', fontsize=15)
    ax2.plot(t_exp2, P_exp, color='green', marker='o')
    ax2.plot(t_exp1, P_model, 'g--')            #maybe /5 to get useful order of magnitude
    ax2.tick_params(axis='y', labelcolor='green', labelsize=15)

    # Show plot
    plt.tight_layout()
    plt.savefig(f'./MichMent_fitted_curves_m3/Mesocosm_{X}_fitting.png')
    plt.show()

    #print(s.get_time())
    #print(time_points)
    return par_data
    
    
def save_parameter_data(parameter_dictionary):
     parameter_df = pd.DataFrame.from_dict({(i): parameter_dict[i] 
                                            for i in parameter_dict.keys()},
                                           orient='index')
     parameter_df.columns=['a', 'b', 'c', 'd', 'e', 'km', 'f']
     parameter_df.to_excel('./MichMent_fitted_curves_m3/parameter_overview.xls', columns=['a', 'b', 'c', 'd', 'e', 'km', 'f'], encoding='utf-8')
     return parameter_df

    
def boxplotter_parameter(parameter_df):
    # parameter analysis: Create a boxplot for parameters
    fig, axes = plt.subplots(2,3)
    df = parameter_df
    for i,el in enumerate(list(df.columns.values)[:-1]):
        a = df.boxplot(el, ax=axes.flatten()[i])
        #ax = fig.add_axes([0, 0, 1, 1])
        #bp = ax.boxplot(parameter_df)
        
        # show plot
        plt.tight_layout()
        plt.savefig('./MichMent_fitted_curves_m3/parameter_boxplot.png')
    
    
"""y0_dict = {"y1": {"N": 1.2, "P": 200.0},    # for TDN
           "y2": {"N": 1.8, "P": 320.0}, 
           "y3": {"N": 1.8, "P": 350.0},
           "y4": {"N": 1.2, "P": 230.0},
           "y5": {"N": 2, "P": 200.0},
           "y6": {"N": 1.3, "P": 200.0},
           "y7": {"N": 1.3, "P": 220.0},
           "y8": {"N": 1.3, "P": 210.0},
           "y9": {"N": 0.75 , "P": 150.0},
           "y10": {"N": 0.9, "P": 190.0},
           "y11": {"N": 1, "P": 280.0},
           "y12": {"N": 0.9, "P": 200.0},
           }  """


y0_dict = {"y1": {"N": 12.5, "P": 220.0},    # for Silicium
           "y2": {"N": 12.5, "P": 280.0}, 
           "y3": {"N": 12.5, "P": 350.0},
           "y4": {"N": 12.5, "P": 220.0},
           "y5": {"N": 12.5, "P": 200.0},
           "y6": {"N": 12.5, "P": 200.0},
           "y7": {"N": 12.5, "P": 220.0},
           "y8": {"N": 12.5, "P": 210.0},
           "y9": {"N": 12.5 , "P": 150.0},
           "y10": {"N": 12.5, "P": 190.0},
           "y11": {"N": 12.5, "P": 280.0},
           "y12": {"N": 12.5, "P": 200.0},
           }  
   
#Time_Fitting_Monod_data(m3, Mesocosm_dict, 2, y0_dict['y2'], 100, 1) 
    
parameter_dict= {}

# loop through Mesocosms and produce fits

for X, y_start in zip (range(1,13), y0_dict):
    if X != 5:
        par_data = Time_Fitting_Monod_data(m3, Mesocosm_dict, X, y0_dict[f'{y_start}'], 15, 1)
        parameter_dict[f'M{X}_data'] = par_data
    else: 
        continue

# save fitting parameters in excel file for analysis
parameter_df = save_parameter_data(parameter_dict)

# produce boxplot per parameter
boxplotter_parameter(parameter_df)
