import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
from IPython.display import display
from matplotlib.ticker import FormatStrFormatter


# Convert date to reduced format
def convertDate(d):
    """function converting dates from format in column 'Date/Time' to reduced date-format (from Anna)"""
    new_date = dt.datetime.strptime(d,"%Y-%m-%dT%H:%M:%S")
    return new_date.date()


#plot M1esocosm-data
def general_plotter(dataframe, MX):
    """function plotting the columns as subplots against the Date"""
    data_env_MX = data_environment.loc[data_environment['Meso'] == MX]   #reduce acutal dataset on one Mesocosm
    #print(data_env_MX.columns)
    #print(data_env_MX)
    data_env_MX = data_env_MX.drop(['DOE [day]', 'Temp descr'], axis=1)
    data_env_MX[data_env_MX.columns].plot(subplots=True, layout=(5,3))
    plt.tight_layout()
    plt.savefig('general environmental plots\\Environment data' + MX)
    #plt.show()
    plt.close()


def phyto_plot(dataframe, X):
    """function plotting the columns as subplots against the Date"""
    data_phy_MX = data_phyto.loc[data_phyto['Meso'] == X]
    #print(data_phy_MX.columns)
    #print(data_phy_MX)
    data_phy_MX[['Date', 'Phytopl C [µg/l] (total phyto C)',
       'Phytopl C [µg/l] (edible phytoplankton C)',
       'Phytopl C [µg/l] (inedible phytoplankton C)' ]].plot(subplots=True, layout=(3,1))
    #plt.ylim(200)
    plt.tight_layout()
    plt.savefig('general phytoplankton plots\\phytopl.' + str(X) + '.png')
    #plt.show()
    plt.close()


# Create data_environment
def init():
    """initialise data sets and bring to a useable format"""
    #import data on environment
    data_environment = pd.read_table\
        ('./Paul_Matthiessen_2021/datasets/mesocosm_2014_environment.tab',
         skiprows=30)                          #if use: "elim_whitespace=True", then KeyError with 'M1' in line 39

    # clearify data/ clean them up
    data_environment = data_environment.dropna()
    data_environment.rename(columns = {'Meso label':'Meso'}, inplace=True)
    data_environment.rename(columns={'Date/Time': 'Date'}, inplace=True)
    data_environment['Date'] = pd.to_datetime(data_environment['Date']).dt.date
    #data_environment['Date'] = data_environment['Date'].apply(convertDate)
    #data_environment = data_environment.drop(['Date'], axis=1) #axis=1 needed to specify on rows
    # import data on phytoplankton
    data_phyto = pd.read_table\
        ('./Paul_Matthiessen_2021/datasets/mesocosm_2014_phytoplankton.tab',
         skiprows=31)

    # clearify data/ clean them up
    data_phyto = data_phyto.dropna()
    data_phyto.rename(columns={'Meso label': 'Meso'}, inplace=True)
    data_phyto.rename(columns={'Date/Time': 'Date'}, inplace=True)
    data_phyto['Date'] = pd.to_datetime(data_phyto['Date']).dt.date
    #data_phyto['Date'] = data_phyto['Date'].apply(convertDate)
    #data_phyto = data_phyto.drop(['Date', 'DOE [day]'], axis=1)

    return data_environment, data_phyto


# Do environment plots here for each meso individually
def create_env_plots_for_each_meso(data_environment):
    """plot development for eacht env. factor for each mesocosm"""
    Mesos = ['M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11', 'M12'] # List with Meso names
    for item in Mesos:
        general_plotter(data_environment, item)


# Do phyto_plots here for each meso individually
def create_phyto_plots(data_phyto):
    """function performing plot creating for phytopl. plots"""
    phyto_plot(data_phyto, 1)
    phyto_plot(data_phyto, 2)
    phyto_plot(data_phyto, 3)
    phyto_plot(data_phyto, 4)
    phyto_plot(data_phyto, 5)
    phyto_plot(data_phyto, 6)
    phyto_plot(data_phyto, 7)
    phyto_plot(data_phyto, 8)
    phyto_plot(data_phyto, 9)
    phyto_plot(data_phyto, 10)
    phyto_plot(data_phyto, 11)
    phyto_plot(data_phyto, 12)


# Compare data_phyto for different mesos
def compare_mesos_plot(data_phyto, group_name, column_name, save_name):
    """plot C for each meso against time, with other mesos"""
    data_phyto_grouped = data_phyto.groupby(group_name)
    data_phyto_grouped[column_name].plot(legend=True)

    ax = plt.gca()  # get current axis of the graph
    n = 2  # Keeps every 2th label
    [l.set_visible(False) for (i, l) in enumerate(ax.xaxis.get_ticklabels()) if i % n != 0]

    plt.title(column_name)
    plt.tight_layout()
    plt.savefig('compare plots\\' + save_name + '.png')  # to plot not average but single Mesos
    plt.close()


def plot_mean_over_mesos(data_phyto, group_name, column_name, save_name):
    """grou data by group_name, then plot mean of a column for these grouping"""
    fig, ax = plt.subplots()
    data_phyto_grouped = data_phyto.groupby(group_name)
    data_phyto_mean = data_phyto_grouped[column_name].mean()
    data_phyto_std = data_phyto_grouped[column_name].std()
    #plt.errorbar(data_phyto[group_name], data_phyto_mean, yerr=data_phyto_std, fmt='o', color='green')
    data_phyto_mean.plot(yerr = data_phyto_std, color='green', linestyle = '', marker = 'o', markersize = 8)
    #plt.title(column_name + ' - mean')
    ax = plt.gca()
    n = 2
    [l.set_visible(False) for (i, l) in enumerate(ax.xaxis.get_ticklabels()) if i % n != 0]
    plt.ylabel('Inedible phytoplankton carbon [µg/l]', fontsize=12)
    plt.xlabel('Date', fontsize=12)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.tight_layout()
    plt.savefig('mean plots\\' + save_name + '.png')
    plt.close()


def make_compare_meso_plots(data_phyto):
    """produce comparison plots"""
    #data_phyto.set_index('Date', inplace=True, )  # drop=False
    compare_mesos_plot(data_phyto, 'Meso', 'Phytopl C [µg/l] (total phyto C)', 'Phytopl C (total)')
    compare_mesos_plot(data_phyto, 'Meso', 'Phytopl C [µg/l] (edible phytoplankton C)', 'Phytopl C (edible)')
    compare_mesos_plot(data_phyto, 'Meso', 'Phytopl C [µg/l] (inedible phytoplankton C)', 'Phytopl C (inedible)')
    plot_mean_over_mesos(data_phyto, 'Date', 'Phytopl C [µg/l] (total phyto C)', 'Phytopl C (total)')
    plot_mean_over_mesos(data_phyto, 'Date', 'Phytopl C [µg/l] (edible phytoplankton C)', 'Phytopl C (edible)')
    plot_mean_over_mesos(data_phyto, 'Date', 'Phytopl C [µg/l] (inedible phytoplankton C)', 'Phytopl C (inedible)')


#group data by env factor and plot all mesos
def env_cat_plot(data_environment, column_name1, column_name2, column_name3):
    """plot all mesos soecified by one column (grouped for mesos) in one plot"""
    
    data_environment = data_environment[data_environment["Meso"].str.contains("M2") == False]
    data_environment = data_environment[data_environment["Meso"].str.contains("M5") == False]
    data_environment = data_environment[data_environment["Meso"].str.contains("M6") == False]
    data_environment = data_environment[data_environment["Meso"].str.contains("M8") == False]
    data_environment = data_environment[data_environment["Meso"].str.contains("M10") == False]
    data_environment = data_environment[data_environment["Meso"].str.contains("M12") == False]
    print(data_environment)
    
    fig, (ax1, ax2, ax3) = plt.subplots(3,1, sharex=True, figsize=(9,10))
    data_environment.groupby('Meso')[column_name1].plot(ax=ax1, legend=False)
    ax1.set_ylabel(f'{column_name1}', fontsize=15)
    ax1.tick_params(axis='both', labelsize=14)
    #ax1.set_yticks(fontsize=12)
    data_environment.groupby('Meso')[column_name2].plot(ax= ax2, legend=False)
    ax2.set_ylabel(f'{column_name2}', fontsize=15)
    ax2.tick_params(axis='both', labelsize=14)
    #ax2.set_yticks(fontsize=12)
    data_environment.groupby('Meso')[column_name3].plot(ax=ax3, legend=False)
    ax3.set_ylabel(f'{column_name3}', fontsize=15)
    ax3.tick_params(axis='both', labelsize=14)
    ax3.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    #ax3.set_yticks(fontsize=12)
    #plt.xticks(fontsize=12)
    ax = plt.gca()
    n = 2
    [l.set_visible(False) for (i, l) in enumerate(ax.xaxis.get_ticklabels()) if i % n != 0]
    plt.xlabel('Date', fontsize=15)
    plt.legend( bbox_to_anchor=(1,1,0,0))
    plt.tight_layout()
    plt.savefig('env-category\\- Triple.png')
    #plt.show()
    plt.close()

def make_env_cat_plots(data_environment):
    """produce environmental plots"""
    #data_environment.set_index('Date', inplace=True)
    env_cat_plot(data_environment, 'Temp [°C]', 'TDN [µmol/l]', 'Si(OH)4 [µmol/l]')
    #env_cat_plot(data_environment, 'Sal', 'Salinity')
    #env_cat_plot(data_environment, 'pCO2 [µatm]', 'CO2 partial pressure')
    #env_cat_plot(data_environment, '[NO3]- + [NO2]- [µmol/l]', 'nitrate and nitrite')
    #env_cat_plot(data_environment, '[NH4]+ [µmol/l]', 'ammonium')
    #env_cat_plot(data_environment, 'TDN [µmol/l]')
    #env_cat_plot(data_environment, 'Si(OH)4 [µmol/l]')
    #env_cat_plot(data_environment, 'O2 [µmol/l]', 'oxygen')
    #env_cat_plot(data_environment, 'POC/PON [mol/mol]', 'POC-PON-ratio')


def plot_mean_over_mesos_filtered(data, group_name, column_name, save_name, list1, list2):
    """function to plot mean over mesos, grouped by data (e.g. temperature separating in two groups)"""
    data_selected1 = data[data.Meso.isin(list1)]
    data_grouped1 = data_selected1.groupby(group_name)
    data_mean1 = data_grouped1[column_name].mean()
    data_std1 = data_grouped1[column_name].std()
    data_mean1.plot(yerr = data_std1, linestyle = '', marker = '.', markersize = 8, label='G1')
    data_selected2 = data[data.Meso.isin(list2)]
    data_grouped2 = data_selected2.groupby(group_name)
    data_mean2 = data_grouped2[column_name].mean()
    data_std2 = data_grouped2[column_name].std()
    data_mean2.plot(yerr=data_std2, linestyle='', marker='.', markersize=8, label='G2')
    plt.title(column_name + ' - mean filtered')
    plt.legend()
    ax = plt.gca()
    n = 2
    [l.set_visible(False) for (i, l) in enumerate(ax.xaxis.get_ticklabels()) if i % n != 0]
    plt.tight_layout()
    plt.savefig('mean filtered\\' + save_name + '.png')
    #plt.show()
    plt.close()


def plot_all_mean_filtered(data_phyto, data_environment):
    """produce mean-plots after temp-groups"""
    G1 = [1, 2, 3, 4, 5, 6]
    G2 = [7, 8, 9, 10, 11, 12]
    G1_with_M = ['M1', 'M2', 'M3', 'M4', 'M5', 'M6']
    G2_with_M = ['M7', 'M8', 'M9', 'M10', 'M11', 'M12']
    plot_mean_over_mesos_filtered(data_phyto, 'Date', 'Phytopl C [µg/l] (total phyto C)', 'Phytopl C (total)', G1, G2)
    plot_mean_over_mesos_filtered(data_phyto, 'Date', 'Phytopl C [µg/l] (edible phytoplankton C)', 'Phytopl C (edible)', G1, G2)
    plot_mean_over_mesos_filtered(data_phyto, 'Date', 'Phytopl C [µg/l] (inedible phytoplankton C)', 'Phytopl C (inedible)', G1, G2)
    plot_mean_over_mesos_filtered(data_environment, 'Date', 'TDN [µmol/l]', 'TDN', G1_with_M, G2_with_M)
    plot_mean_over_mesos_filtered(data_environment, 'Date', 'Si(OH)4 [µmol/l]', 'silicium', G1_with_M, G2_with_M)
    plot_mean_over_mesos_filtered(data_environment, 'Date', 'Sal', 'Salinity', G1_with_M, G2_with_M)
    plot_mean_over_mesos_filtered(data_environment, 'Date', 'pCO2 [µatm]', 'CO2 partial pressure', G1_with_M, G2_with_M)
    plot_mean_over_mesos_filtered(data_environment, 'Date', '[NO3]- + [NO2]- [µmol/l]', 'nitrate and nitrite', G1_with_M,
                                  G2_with_M)
    plot_mean_over_mesos_filtered(data_environment, 'Date', 'O2 [µmol/l]', 'oxygen', G1_with_M, G2_with_M)
    plot_mean_over_mesos_filtered(data_environment, 'Date', 'POC/PON [mol/mol]', 'POC-PON-ratio', G1_with_M, G2_with_M)


def nut_and_phyto(data_environment, data_phyto, X, MX, column_name1, column_name2, save_name):
    data_env_MX = data_environment.loc[data_environment['Meso'] == MX]
    data_phy_MX = data_phyto.loc[data_phyto['Meso'] == X]
    ax = data_env_MX[column_name1].plot(color='blue')
    ax.set_xlabel('Date', fontsize=15)
    ax.set_ylabel(f'{column_name1}', color='blue', fontsize=15)
    n = 2
    [l.set_visible(False) for (i, l) in enumerate(ax.xaxis.get_ticklabels()) if i % n != 0]
    ax2 = data_phy_MX[column_name2].plot(secondary_y=True, color='green')
    ax2.set_ylabel('inedible Phytoplankton carbon [µg/l]', color='green', fontsize=15)
    #plt.title(save_name + ' nutrient and phyto')
    ax.tick_params(axis='y', labelsize=15)
    ax.tick_params(axis='x', labelsize=12)
    ax2.tick_params(axis='y', labelsize=15)
    plt.tight_layout()
    plt.savefig('nutrient and phyto\\' + save_name + '.png')
    #plt.show()
    plt.close()

def make_nut_and_phyto(data_environment, data_phyto):
    for X in range(1, 13):
        nut_and_phyto(data_environment, data_phyto, X, 'M'+str(X), 'TDN [µmol/l]', 'Phytopl C [µg/l] (inedible phytoplankton C)',
                  'Mesocosm ' +str(X) +' comp nut (TDN) phyto')
    for X in range(1, 13):
        nut_and_phyto(data_environment, data_phyto, X, 'M'+str(X), 'Si(OH)4 [µmol/l]', 'Phytopl C [µg/l] (inedible phytoplankton C)',
                  'Mesocosm ' +str(X) +' comp nut (Silicium) phyto')


 # um durch alle environment categories zu loopen: LIste für Env. cat column-Names anlegen und durch liste loopen:-)



"""
Main part
"""
data_environment, data_phyto = init()
#create_env_plots_for_each_meso(data_environment)
#create_phyto_plots(data_phyto)
data_environment.set_index('Date', inplace=True)
data_phyto.set_index('Date', inplace=True)
#make_compare_meso_plots(data_phyto)
make_env_cat_plots(data_environment)
#plot_all_mean_filtered(data_phyto, data_environment)
#make_nut_and_phyto(data_environment, data_phyto)
plot_mean_over_mesos(data_phyto, 'Date', 'Phytopl C [µg/l] (inedible phytoplankton C)', 'Try')
