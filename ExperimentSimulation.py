import numpy as np
import matplotlib.pyplot as plt
import scipy
from modelbase.ode import Model, Simulator


from hubbertmodel import m
from HuppertMonod_new import get_huppert_v1
from HuppertMonod_new import get_huppert_v3




"""SIMULATION 0: Huppert model"""

m1=get_huppert_v1()
stoi_matrix = m1.get_stoichiometric_df()
#print(f"stoi= {stoi_matrix}")

s0 = Simulator(m)
# set initial conditions
y0 = {"P": 0.005, "N": 0.0005}           #result "nearest" to Fig 3 from paper: P = 0.005
s0.initialise(y0)
# simulate
s0.simulate(365, 1000)
#print(s2.results[0][:,0])
#plot simulation to create figure 3
#s2.plot(xlabel ='time [days]', ylabel='population size/ nutrient mass [(m)g/m^3]')
plt.plot(s0.get_time(), s0.get_variable('P')*1000, label="phytoplankton", color="green")
plt.plot(s0.get_time(), s0.get_variable('N')*1000/5, label="nutrient", color="blue")
plt.ylabel("biomass pythoplankton (P) [g/m$^3$], nutrient (N) [mg/m$^3$]")
plt.xlabel("time [days]")
plt.legend()
plt.tight_layout()
plt.savefig("./general_result_Huppert.png")
#plt.show()
plt.close()




"""SIMULATION 1: Hupppert model with first light investigation """

y0 = {"P": 0.005, "N": 0.0005}

fig, ((ax1, ax2)) = plt.subplots(1, 2)
plt.suptitle("Varying light parameter influence on Hubbert Equations")
#plt.ylabel("biomass pythoplankton (P) [g/m^3], nutrient (N) [mg/m^3]")
ax1.set_xlabel("time [days]")
ax2.set_xlabel("time [days]")
ax1.set_ylabel("biomass pythoplankton (P) [g/$m^3$]")
ax2.set_ylabel("nutrient (N) [mg/$m^3$]")

for light in [0.6, 0.8, 1., 1.2, 1.4]:
    # create a simulation
    s1 = Simulator(m)
    s1.update_parameter("l", light)
    # set initial conditions
    s1.initialise(y0)
    # simulate
    s1.simulate(800, 1000)
    ax1.plot(s1.get_time(), s1.get_variable('P')*1000, label=f"l={light}")
    ax2.plot(s1.get_time(), s1.get_variable('N') * 1000 / 5, label=f"l={light}")

ax1.legend()
ax2.legend()
plt.tight_layout()
plt.savefig("./light exploration/light_variation1.png")
#plt.show()
plt.close()




"""SIMULATION 2: Huppert model with Monod-uptake (uptake b and c separate)"""
#m1.update_parameter("a", 0.00075)
s2 = Simulator(m1)
# set initial conditions
y0 = {"P": 200.05, "N": 2.05}
s2.initialise(y0)
#s2.update_parameters({'a': 0.46855187768266615, 'b': 1.0078233407368768, 'c': 0.3135427894804985, 'd': -0.06004999684836367, 'e': -0.005708899245528494, 'l': 1.0, 'cl': 0.3135427894804985})
s2.update_parameters({'a': 0.75, 'b': 1000, 'c': 1000, 'd': 0.1, 'e': 0, 'l': 1000, 'cl': 1000})
# simulate
s2.simulate(700, 1000)
#print(s2.results[0][:,0])
#plot simulation to create figure 3
#s2.plot(xlabel ='time [days]', ylabel='population size/ nutrient mass [(m)g/m^3]')
plt.plot(s2.get_time(), s2.get_variable('P')*1000, label="phytoplankton", color="green")
plt.plot(s2.get_time(), s2.get_variable('N')*1000/5, label="nutrient", color="blue")
plt.ylabel("biomass pythoplankton (P) [g/m$^3$], nutrient (N) [mg/m$^3$]")
plt.xlabel("time [days]")
plt.legend()
plt.tight_layout()
#plt.savefig("./Monod kinetics experiment/Monod with B and C.png")
#plt.show()
plt.close()



"""SIMULATION 3: Huppert model with Monod-uptake (uptake b and c separate), updated by nutrient-proportion (taken out '/5')"""
s3 = Simulator(m1)
# set initial conditions
y0 = {"P": 0.05, "N": 0.05}         #changed N from 0.0005 to 0.05 starting condition
s3.initialise(y0)
# simulate
s3.simulate(365, 1000)
#print(s2.results[0][:,0])
#plot simulation to create figure 3
#s2.plot(xlabel ='time [days]', ylabel='population size/ nutrient mass [(m)g/m^3]')
plt.plot(s3.get_time(), s3.get_variable('P')*1000, label="phytoplankton", color="green")
plt.plot(s3.get_time(), s3.get_variable('N')*1000, label="nutrient", color="blue")          # taken out 1000/5 in this line at point variable
plt.ylabel("biomass pythoplankton (P) [g/m$^3$], nutrient (N) [mg/m$^3$]")
plt.xlabel("time [days]")
plt.legend()
plt.tight_layout()
plt.savefig("./Monod kinetics experiment/Monod with B and C correct proportion.png")
#plt.show()
plt.close()



"""SIMULATION 4: play around with data from dataexploration"""
#m1.update_parameter("a", 0.001)
#m1.update_parameter("b", 0.1)
#m1.update_parameter("c", 0.5)
#m1.update_parameter("d", 1)
#m1.update_parameter("e", 0.01)
s3 = Simulator(m1)
# set initial conditions
y0 = {"P": 1, "N":2}         #changed N from 0.0005 to 0.05 starting condition
s3.initialise(y0)
# simulate
s3.simulate(365, 1000)
#print(s2.results[0][:,0])
#plot simulation to create figure 3
#s2.plot(xlabel ='time [days]', ylabel='population size/ nutrient mass [(m)g/m^3]')
plt.plot(s3.get_time(), s3.get_variable('P')*1000, label="phytoplankton", color="green")
plt.plot(s3.get_time(), s3.get_variable('N')*1000/5, label="nutrient", color="blue")          # taken out 1000/5 in this line at point variable
plt.ylabel("biomass pythoplankton (P) [g/m$^3$], nutrient (N) [mg/m$^3$]")
plt.xlabel("time [days]")
plt.legend()
plt.tight_layout()
#plt.savefig("./Monod kinetics experiment/dataexploration.png")
#plt.show()
plt.close()



"""SIMULATION 5: Huppert model with Monod-uptake TIME (uptake b and c separate)"""
#m1.update_parameter("a", 0.00075)
s2 = Simulator(m1)
# set initial conditions
y0 = {"P": 0.005, "N": 0.0005}
s2.initialise(y0)
# simulate
s2.simulate(624, 1000)
#print(s2.results[0][:,0])
#plot simulation to create figure 3
#s2.plot(xlabel ='time [days]', ylabel='population size/ nutrient mass [(m)g/m^3]')
plt.plot(s2.get_time(), s2.get_variable('P')*1000, label="phytoplankton", color="green")
plt.plot(s2.get_time(), s2.get_variable('N')*1000, label="nutrient", color="blue")
plt.ylabel("biomass pythoplankton (P) [g/m$^3$], nutrient (N) [mg/m$^3$]")
plt.xlabel("time [days]")
plt.legend()
plt.tight_layout()
plt.savefig("./Monod kinetics experiment/Time_playaround.png")
#plt.show()
plt.close()



"""SIMULATION 6: Huppert model with Michaelis-Menten uptake general plot"""
m3=get_huppert_v3()
stoi_matrix = m3.get_stoichiometric_df()
print(f"stoi= {stoi_matrix}")
m3.update_parameter("km", 1.2)
s3 = Simulator(m3)
# set initial conditions
y0 = {"P": 0.005, "N": 0.0005}           
s3.initialise(y0)
# simulate
s3.simulate(365, 1000)
#plot simulation to create figure 3
#s2.plot(xlabel ='time [days]', ylabel='population size/ nutrient mass [(m)g/m^3]')
plt.plot(s3.get_time(), s3.get_variable('P')*1000, label="phytoplankton", color="green")
plt.plot(s3.get_time(), s3.get_variable('N')*1000/5, label="nutrient", color="blue")
plt.ylabel("biomass pythoplankton (P) [g/m$^3$]  \n nutrient (N) [mg/m$^3$]", fontsize=15)
plt.xticks(fontsize=15)
plt.xlabel("time [days]", fontsize=15)
plt.yticks(fontsize=15)
plt.legend(fontsize=15)
plt.tight_layout()
plt.savefig("./MichMent kinetics experiment/general_result_Huppert_MichMent_km12.png")
#plt.show()
plt.close()

"""SIMULATION 7: Parameter investigation """
m=m3
m.update_parameters({'a': 0.00075 * 9000, 'b': 0.5, 'c': 1, 'd': 0.1, 'e': 0, 'km': 0.1* 10000})
s = Simulator(m)

s.initialise({"N": 12.5, "P": 220.0})

s.simulate(30)

s.plot()
plt.show()