#!/usr/bin/env python
# coding: utf-8

# ### Step 0
# Download neccesary packages for 
#  - plotting
#  - data analysis
#  - integration
#  - fitting
#  - numerical calculations

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import scipy

from modelbase.ode import Model, Simulator

import pandas as pd
import datetime as dt
from IPython.display import display
from lmfit import minimize, Parameters, Parameter, report_fit

from numpy.typing import NDArray
from scipy.optimize import minimize


# ## Step 1 
# Read the experimental data for nutrients concentration

# In[2]:


# import data on nutrients -> column TDN
data_environment = pd.read_table        ('datasetexploration/Paul_Matthiessen_2021/datasets/mesocosm_2014_environment.tab',
         skiprows=30)                          #if use: "elim_whitespace=True", then KeyError with 'M1' in line 39

# clearify data/ clean them up
data_environment = data_environment.dropna()
data_environment.rename(columns = {'Meso label':'Meso'}, inplace=True)
data_environment['Date/Time'] = pd.to_datetime(data_environment['Date/Time']).dt.date


# In[15]:


data_env_M1 = data_environment.loc[data_environment['Meso'] == 'M1']


# In[16]:


t_exp1 = data_env_M1['DOE [day]'][2:] 
N_exp = data_env_M1['TDN [µmol/l]'][2:] 


# In[6]:


t_exp1


# ### Step 2
# Read the experimental data for phytoplankton concentrations

# In[17]:


# import data on phytoplankton -> column Phytopl. C
data_phyto = pd.read_table    ('./datasetexploration/Paul_Matthiessen_2021/datasets/mesocosm_2014_phytoplankton.tab',
     skiprows=31)

data_phyto = data_phyto.dropna()
data_phyto.rename(columns={'Meso label': 'Meso'}, inplace=True)
data_phyto['Date/Time'] = pd.to_datetime(data_phyto['Date/Time']).dt.date


# In[18]:


data_phyto_M1 = data_phyto.loc[data_phyto['Meso'] == 1]


# In[9]:


t_exp2 = data_phyto_M1['DOE [day]'][1:] 



# ### Step 3
# Visualise the two on a graph

# In[19]:


fig, ax1 = plt.subplots() 

ax1.set_xlabel('days passed from the beginning of the experiment') 
ax1.set_ylabel('Nutrients', color = 'red') 
ax1.plot(t_exp1, N_exp, color = 'red') 
ax1.tick_params(axis ='y', labelcolor = 'red') 
  
# Adding Twin Axes

ax2 = ax1.twinx() 
  
ax2.set_ylabel('Edible Phytoplankton', color = 'blue') 
ax2.plot(t_exp2, P_exp, color = 'blue') 
ax2.tick_params(axis ='y', labelcolor = 'blue') 
 
# Show plot

plt.show()


# In[29]:


t_exp1


# ### Step 4
# Import the model (in modelbase)

# In[21]:


from HuppertwithMonod import m1


# In[22]:


m1.stoichiometries


# ### Step 5
# Define function that will fit parameters to the experimental data set

# In[24]:


def fit_time_series(
    m: Model,
    p0: dict[str, float],
    y0: dict[str, float],
    data: NDArray,
    time_points: NDArray,
) -> dict[str, float]:
    par_names = list(p0.keys())
    x0 = list(p0.values())
    assert len(data) == len(time_points)

    def residual(par_values: NDArray, data: NDArray, time_points: NDArray):
        s = Simulator(m.copy())
        s.update_parameters(dict(zip(par_names, par_values)))
        s.initialise(y0)
        _, y = s.simulate(time_points=time_points)
        if y is None:
            return np.inf
        return np.sqrt(np.mean(np.square(data - y)))

    return dict(zip(par_names, minimize(residual, x0=x0, args=(data, time_points)).x))


# In[51]:


y0 = {"N": 1.0, "P": 0.0}

s = Simulator(m1)
s.initialise(y0)

time_points = t_exp1.to_numpy()

assert y is not None

print(fit_time_series(m1, {"a": 0.00075, "b": 1, "c": 1, "d": 0.1, "e": 0,  "l": 1.}, y0, N_exp.to_numpy(), time_points))


# In[ ]:




